package com.adamkorzeniak.utils.logging.test.utils;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;

import java.util.ArrayList;
import java.util.List;

public class TestAppender extends AppenderBase<ILoggingEvent> {

    private static final List<ILoggingEvent> events = new ArrayList<>();

    public static List<ILoggingEvent> getCopyOfEvents() {
        return new ArrayList<>(events);
    }

    public static void clear() {
        events.clear();
    }

    @Override
    protected void append(ILoggingEvent e) {
        events.add(e);
    }
}
