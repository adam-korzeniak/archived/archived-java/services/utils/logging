package com.adamkorzeniak.utils.logging.filter;

import lombok.Value;

@Value
public class BeforeRequestData {

    private final String method;
    private final String url;

    @Override
    public String toString() {
        return String.format("%s %s", method, url);
    }
}
