package com.adamkorzeniak.utils.logging.filter;

import lombok.Value;

@Value
public class AfterResponseData {

    private final BeforeRequestData beforeData;
    private final String requestBody;
    private final int status;
    private final String responseBody;

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(beforeData.toString());
        if (requestBody != null && !requestBody.isBlank()) {
            result.append("\nRequest Body: ").append(requestBody);
        }
        result.append("\nStatus: ").append(status);
        if (responseBody != null && !responseBody.isBlank()) {
            result.append("\nResponse Body: ").append(responseBody);
        }
        return result.toString();
    }


}
