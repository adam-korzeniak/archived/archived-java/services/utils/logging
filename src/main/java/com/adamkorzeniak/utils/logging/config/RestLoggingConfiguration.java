package com.adamkorzeniak.utils.logging.config;

import com.adamkorzeniak.utils.logging.filter.RestApiRequestLogger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RestLoggingConfiguration {

    @Bean
    public RestApiRequestLogger restApiRequestLogger() {
        return new RestApiRequestLogger();
    }

}
